# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

DISTRO = "oniro-openharmony-linux"
DISTRO_NAME = "Oniro/OpenHarmony Linux Distro"
DISTRO_VERSION = "1.99.99"

# OpenHarmony OS version
require include/openharmony.inc
DISTRO_FEATURES:append = " openharmony"

# LLVM/Clang toolchain
TOOLCHAIN = "clang"
RUNTIME = "llvm"

# musl libc
TCLIBC = "musl"

# Include clang in generated SDK toolchain
CLANGSDK = "1"
# Include static libraries in SDK
SDKIMAGE_FEATURES:append = " staticdev-pkgs"

ONIRO_OPENHARMONY_DEFAULT_DISTRO_FEATURES = "ipv4 ptest"
DISTRO_FEATURES ?= "${ONIRO_OPENHARMONY_DEFAULT_DISTRO_FEATURES}"

INIT_MANAGER = "systemd"

# Linux kernel version
PREFERRED_VERSION_linux-yocto = "5.10.%"

# Attempt to isolate the buidl system from the host distribution's C library
require conf/distro/include/yocto-uninative.inc
INHERIT += "uninative"

# Workaround for missing host tools in do_testimage when not adding testimage to
# IMAGE_CLASSES, but doing a direct `inherit testimage` in image recipes
# instead.
HOSTTOOLS += "ip ping ps scp ssh stty"
